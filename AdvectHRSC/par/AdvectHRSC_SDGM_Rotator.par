ActiveThorns = "
AdvectHRSC
ADMBase
Boundary
Carpet
CarpetIOASCII
CarpetIOBasic
CarpetIOHDF5
CarpetIOScalar
CarpetLib
CarpetReduce
CarpetSlab
CartGrid3d
CoordBase
HRSCCore
IOUtil
LoopControl
MoL
NanChecker
Periodic
SDGMGrid
Slab
SymBase
Time
"

#############################################################
# Grid
#############################################################

SDGMGrid::xmin                          = -0.75
SDGMGrid::ymin                          = -0.75
SDGMGrid::zmin                          = -1

SDGMGrid::xmax                          = 0.75
SDGMGrid::ymax                          = 0.75
SDGMGrid::zmax                          = 1

# Note that this has to be equal to the hard-coded value
# in AdvectHRSC
SDGMGrid::sdgm_order                    = 5

SDGMGrid::nelem_x                       = 20
SDGMGrid::nelem_y                       = 20
SDGMGrid::nelem_z                       = 1

CartGrid3D::type                        = "multipatch"

#############################################################
# Carpet
#############################################################

Carpet::ghost_size                      = 1
Carpet::granularity                     = 6   # order+1
Carpet::granularity_boundary            = 1
Carpet::domain_from_multipatch          = "yes"
Carpet::max_refinement_levels           = 1
#Carpet::init_each_timelevel             = "yes"
Carpet::num_integrator_substeps         = 4

#############################################################
# Time integration
#############################################################

Cactus::terminate                     = "time"
Cactus::cctk_final_time               = 10

Time::timestep_method                 = "given"
# From Cockburn and Shu 2001
# CFL = 0.073
Time::timestep                        = 0.001

MethodOfLines::ode_method             = "RK4"
MethodOfLines::MoL_Intermediate_Steps = 4
MethodOfLines::MoL_Num_Scratch_Levels = 1
MethodOfLines::MoL_NaN_Check          = "yes"

#############################################################
# AdvectHRSC
#############################################################

AdvectHRSC::id_phi_type         = "gaussian"
AdvectHRSC::id_k[0]             = 1
AdvectHRSC::id_k[1]             = 0
AdvectHRSC::id_k[2]             = 0
AdvectHRSC::id_vel_type         = "rigid_rotation"

AdvectHRSC::id_phi_amplitude    = 1

AdvectHRSC::id_lowbound[0]      = 0.25
AdvectHRSC::id_upbound[0]       = 0.5
AdvectHRSC::id_lowbound[1]      = -0.125
AdvectHRSC::id_upbound[1]       = 0.125
AdvectHRSC::id_lowbound[2]      = -1
AdvectHRSC::id_upbound[2]       = 1

AdvectHRSC::id_gpos[0]          = 0.375
AdvectHRSC::id_isigma[0]        = 100
AdvectHRSC::id_isigma[1]        = 100
AdvectHRSC::id_isigma[2]        = 0

AdvectHRSC::bc_type             = "none"
Periodic::periodic              = "yes"

#############################################################
# Output
#############################################################

IO::out_dir                         = $parfile
IO::out_fileinfo                    = "all"

CarpetIOBasic::outInfo_every        = 5
CarpetIOBasic::outInfo_vars         = "AdvectHRSC::phi"

CarpetIOScalar::outscalar_every     = 15
CarpetIOScalar::outscalar_vars      = "AdvectHRSC::error"

IOASCII::out1D_criterion            = "time"
IOASCII::out1D_dt                   = 0.1
IOASCII::out1D_x                    = "yes"
IOASCII::out1D_y                    = "yes"
IOASCII::out1D_vars                 = "AdvectHRSC::phi Grid::x Grid::y"

CarpetIOHDF5::out2D_vars            = "AdvectHRSC::phi Grid::x Grid::y"
CarpetIOHDF5::out2D_criterion       = "time"
CarpetIOHDF5::out2D_dt              = 0.1
CarpetIOHDF5::out2D_xy              = "yes"
CarpetIOHDF5::out2D_xz              = "no"
CarpetIOHDF5::out2D_yz              = "no"

CarpetIOASCII::out_precision        = 19
CarpetIOASCII::out3D_ghosts         = "yes"

# vim: set ft=sh tabstop=20 noexpandtab :
